# Hot To Run the Project


Requirements: Docker or Java and Maven


## Docker Compose

1. ``docker compose build`` in root folder
2. ``docker compose up`` to run project
3. Go to [localhost:6868](http://localhost:6868/)


## Java and Maven
1. Run Spring Boot App
2. Go to [localhost:8080](http://localhost:8080/)

## Logging in

### Admin Account/HSE Worker

1. go to /login
2. enter admin email "admin@gmail.com"
3. enter admin password "admin"

### User Account

1. go to /login
2. enter admin email "user@gmail.com"
3. enter admin password "admin"

### Register Account

1. go to /register
2. Enter Details
3. Press Register
4. Go to /login
5. Enter Details and Log in

Email Note: Must contain @
Date Of Birth Note: Must be older than 18
Password Note: Password Must contain more than 8 symbols, a capital and both letters and numbers