package com.example.demo.common;

import com.example.demo.model.AppointmentBooking;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

public class BookingCart {
    @SuppressWarnings("unchecked")
    public static Map<String, AppointmentBooking> getBookingCart(HttpSession session) {
        Map<String, AppointmentBooking> bookingCart = new HashMap<>();
        if (session.getAttribute("existingBookingCart") != null) {
            bookingCart = (Map<String, AppointmentBooking>) session.getAttribute("existingBookingCart");
        }
        return bookingCart;
    }

    public static int getBookingCartCount(Map<String, AppointmentBooking> bookingCart) {
        int count=0;
        count = bookingCart.values().size();
        return count;
    }
}