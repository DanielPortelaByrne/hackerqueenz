package com.example.demo.service;

import javax.transaction.Transactional;

import com.example.demo.model.Appointment;
import com.example.demo.model.AppointmentBooking;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class AppointmentBookingService {

    public AppointmentBooking createAppointmentBooking(Appointment appointment) {
        AppointmentBooking appointmentBooking = new AppointmentBooking();
        appointmentBooking.setAppointment(appointment);
        return appointmentBooking;
    }
}
