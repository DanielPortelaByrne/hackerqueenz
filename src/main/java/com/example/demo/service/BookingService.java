package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import com.example.demo.model.Booking;
import com.example.demo.model.BookingStatus;
import com.example.demo.model.Confirmation;
import com.example.demo.model.User;
import com.example.demo.repos.BookingRepository;
import com.example.demo.repos.ConfirmationRepository;
import javassist.tools.rmi.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class BookingService {
    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private ConfirmationRepository confirmationRepository;


    public Booking saveBooking(Booking booking) {
        return bookingRepository.save(booking);
    }

    public Booking saveNewBooking(Booking booking) {
        Confirmation confirmation = new Confirmation();
        confirmation.setBooking(booking);
        booking.setConfirmation(confirmationRepository.save(confirmation));
        return bookingRepository.save(booking);
    }

    public Booking updateBookingStatus(String id, BookingStatus status) throws ObjectNotFoundException {
        Booking updateBooking = getBooking(id);
        updateBooking.setStatus(status);
        return bookingRepository.save(updateBooking);
    }

    public List<Booking> getBookingsForClient(User client) {
        return bookingRepository.findByClient(client);
    }

    public List<Booking> getAllBookings() {
        return bookingRepository.findAll();
    }

    public Booking getBooking(String id) throws ObjectNotFoundException {
        Booking booking;
        booking = bookingRepository.findById(id);
        return booking;
    }
}
