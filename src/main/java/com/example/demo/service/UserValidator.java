package com.example.demo.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import com.example.demo.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {
    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    private static boolean isInFile(String password)  {
        try {
            return Files.lines(Paths.get("src/main/resources/static/blacklist.txt")).anyMatch(l -> l.contains(password));
        } catch (IOException e) {
            
            e.printStackTrace();
        }

        return false;
    }

    public boolean checkIfInPassword(List<String> letters,String password) {

        for (String letter : letters) {

            if(password.contains(letter)) {
                return true;
            }
            
        }

        return false;
    }
    
    public boolean containsUpperCase(String password) {
        
        for (int i = 0; i < password.length(); i++) {
            
            if(Character.isUpperCase(password.charAt(i))) {
                
                return true;
            }
            
        }
        
        return false;
    }
    public boolean containsLowerCase(String password) {

        for (int i = 0; i < password.length(); i++) {

            if(Character.isLowerCase(password.charAt(i))) {

                return true;
            }
            
        }

        return false;
    }



    public boolean validatePassword(String password)  {
        
        List<String> symbols = Arrays.asList("~","`","!", "@","#","$","%","^","&","*","(",")","_","-","+","=","{","[","}","]","|","\\",";","<",">",".","?","/");

        if(!containsLowerCase(password)) {
            return false;
        }

        if(!containsUpperCase(password)) {
            return false;
        }

        if(isInFile(password)) {
            return false;
        }
        
        if(!checkIfInPassword(symbols, password)) {
            return false;
        }

        return true;
    }

    @Override
    public void validate(Object o, Errors errors) {

        User user = (User) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty");
        if (user.getUsername().length() < 6 || user.getUsername().length() > 32) {
            errors.rejectValue("email", "Size.userForm.username");
        }
        
        if (userService.findByEmail(user.getEmail()) != null) {
            errors.rejectValue("email", "Duplicate.userForm.username");
        }
        
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        if (user.getPassword().length() < 8 || user.getPassword().length() > 32) {
            errors.rejectValue("password", "Size.userForm.password");
        }
        
        if (!validatePassword(user.getPassword())) {
            errors.rejectValue("password", "Size.userForm.password");
        }
        
        if (!user.getPasswordConfirm().equals(user.getPassword())) {
            errors.rejectValue("passwordConfirm", "Diff.userForm.passwordConfirm");
        }
        if (!user.getPasswordConfirm().equals(user.getPassword())) {
            errors.rejectValue("passwordConfirm", "Diff.userForm.passwordConfirm");
        }
    }
}