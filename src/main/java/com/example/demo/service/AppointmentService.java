package com.example.demo.service;

import com.example.demo.model.Appointment;
import com.example.demo.repos.AppointmentRepository;
import javassist.tools.rmi.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
public class AppointmentService {
    @Autowired
    private AppointmentRepository appointmentRepository;

    public Appointment createAppointment(Appointment appointment) {
        return appointmentRepository.save(appointment);
    }

    public Appointment findByName(String name) {
        return appointmentRepository.findByName(name);
    }

    public Appointment updateAppointment(Appointment appointment){
        return appointmentRepository.save(appointment);
    }

    public Appointment updateAppointmentCount(String id, int count) throws ObjectNotFoundException {
        Appointment updateAppointment = getAppointment(id);
        updateAppointment.setAvailableAppointmentCount(count);
        return appointmentRepository.save(updateAppointment);
    }

    public Appointment getAppointment(String id) throws ObjectNotFoundException {
        Appointment appointment;
        appointment = appointmentRepository.findById(id);
        return appointment;
    }

    public List<Appointment> getAllAvailableAppointments() {
        List<Appointment> appointments = appointmentRepository.findAll();
        List<Appointment> availableAppointments = new ArrayList<>();
        for(Appointment availableAppointment : appointments) {
            if(availableAppointment.getAvailability()) {
                availableAppointments.add(availableAppointment);
            }
        }
        return availableAppointments;
    }
}