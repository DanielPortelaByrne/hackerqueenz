package com.example.demo.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.example.demo.common.BookingCart;
import com.example.demo.model.*;
import com.example.demo.repos.AppointmentBookingRepository;
import com.example.demo.service.AppointmentService;
import com.example.demo.service.BookingService;
import com.example.demo.service.UserService;
import javassist.tools.rmi.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class BookingController {
    @Autowired
    private BookingService bookingService;

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private AppointmentBookingRepository appointmentBookingRepository;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/confirmation-confirmed")
    public String createBooking(Model model, HttpSession session, Authentication auth) throws ObjectNotFoundException, ParseException {
        User client = getUserFromEmail(auth.getName());

        List<Booking> bookings = getBookingFromUser(client);

        // Map<Long, AppointmentBooking> bookingCart = BookingCart.getBookingCart(session);


        List<AppointmentBooking> appointmentBookings = new ArrayList<>(BookingCart.getBookingCart(session).values());

        if(bookings != null) {
            if(bookings.size()==2){
                model.addAttribute("error", "You cannot make another booking if you are already fully vaccinated");
                return "confirmation";
            }
            for (Booking currentBooking : bookings) {
                BookingStatus status = currentBooking.getStatus();

                if (status == BookingStatus.CONFIRMED || status == BookingStatus.PENDING) {
                    model.addAttribute("error", "You cannot make a booking if you already have an appointment booked");
                    return "confirmation";
                }
                for (AppointmentBooking appointmentBooking : appointmentBookings) {
                    if (appointmentBooking.getAppointment().getName().equals("FIRST DOSE") && status == BookingStatus.PAST) {
                        model.addAttribute("error", "You cannot book a first dose if you have already received a vaccine");
                        return "confirmation";
                    }
                    for (int k=0; k<currentBooking.getAppointmentBookings().size(); k++)
                    {
                        Date prevDate=new SimpleDateFormat("dd/MM/yyyy").parse(currentBooking.getAppointmentBookings().get(k).getAppointment().getDate());
                        Date nextDate=new SimpleDateFormat("dd/MM/yyyy").parse(appointmentBooking.getAppointment().getDate());
                        long diffInMillis = Math.abs(nextDate.getTime() - prevDate.getTime());
                        long diff = TimeUnit.DAYS.convert(diffInMillis, TimeUnit.MILLISECONDS);

                        System.out.println(nextDate + " - " + prevDate + " = " + diff);

                        if (appointmentBooking.getAppointment().getName().equals("SECOND DOSE") && (diff < 21)) {
                            System.out.println("You can only receive your second dose at least 3 weeks after you received the first dose");
                            model.addAttribute("error", "You can only receive your second dose at least 3 weeks after you received the first dose");
                            return "confirmation";
                        }
                    }
                }
            }
            Booking booking = new Booking();
            booking.setBookingDate(java.util.Calendar.getInstance().getTime());
            booking.setStatus(BookingStatus.PENDING);
            booking.setVaccine(Vaccine.Pfizer);
            Booking savedBooking = bookingService.saveBooking(booking);
            String bookingId = savedBooking.getId();

            booking.addAppointmentBookingList(updateAppointmentBookings(appointmentBookings, bookingId));
            booking.setClient(client);

            bookingService.saveNewBooking(booking);
            session.setAttribute("existingBookingCart", new HashMap<Long, AppointmentBooking>());

            return "redirect:/bookings";
        }
        else {
            for (AppointmentBooking appointmentBooking : appointmentBookings) {
                if (appointmentBooking.getAppointment().getName().equals("SECOND DOSE")) {
                    model.addAttribute("error", "You can only receive your second dose if you have already received the first dose");
                    return "confirmation";
                }
            }
            Booking booking = new Booking();
            booking.setBookingDate(java.util.Calendar.getInstance().getTime());
            booking.setStatus(BookingStatus.PENDING);
            booking.setVaccine(Vaccine.Pfizer);
            Booking savedBooking = bookingService.saveBooking(booking);
            String bookingId = savedBooking.getId();

            booking.addAppointmentBookingList(updateAppointmentBookings(appointmentBookings, bookingId));
            booking.setClient(client);

            bookingService.saveNewBooking(booking);
            session.setAttribute("existingBookingCart", new HashMap<Long, AppointmentBooking>());

            return "redirect:/bookings";
        }
    }

    @RequestMapping(value = "/bookings")
    public String bookings(Authentication auth, Model model) {
        if(auth.getName().equals("admin")) {
            return "redirect:/admin/bookings";
        }
        List<Booking> bookings = bookingService.getBookingsForClient(getUserFromEmail(auth.getName()));
        model.addAttribute("bookings", bookings);
        return "/bookings";
    }

    @RequestMapping(value = "/admin/bookings")
    public String allBookings(Authentication auth, Model model) {

        List<Booking> bookings = bookingService.getAllBookings();
        model.addAttribute("bookings", bookings);
        return "/bookings";
    }

    @RequestMapping("/admin/bookings/{id}/change_status")
    public String changeAppointmentQuantity(HttpServletRequest req, @PathVariable("id") String id, @RequestParam("status") String status, Model model,
                                            HttpSession session) throws ObjectNotFoundException {

        //decode ID first
        Booking booking = bookingService.getBooking(id);

        List<String> userOptions = Arrays.asList("CONFIRMED","CANCELLED");

        if(!req.isUserInRole("ROLE_ADMIN") && !userOptions.contains(status) ) {

            return "redirect:/";

        }
        
        booking.setStatus(BookingStatus.valueOf(status));
        bookingService.saveBooking(booking);
        return "redirect:/admin/bookings";
    }

    @RequestMapping("/admin/bookings/{id}/change_vaccine")
    public String changeVaccineQuantity(@PathVariable("id") String id, @RequestParam("vaccine") String vaccine, Model model,
                                        HttpSession session) throws ObjectNotFoundException {
        Booking booking = bookingService.getBooking(id);
        booking.setVaccine(Vaccine.valueOf(vaccine));
        bookingService.saveBooking(booking);
        return "redirect:/admin/bookings";
    }

    private User getUserFromEmail(String email) {
        return userService.findByEmail(email);
    }

    private List<Booking> getBookingFromUser(User client) {
        return bookingService.getBookingsForClient(client);
    }

    private List<AppointmentBooking> updateAppointmentBookings(List<AppointmentBooking> appointmentBookings, String bookingId) throws ObjectNotFoundException {
        for (AppointmentBooking ab : appointmentBookings) {
            ab.setBookingId(bookingId);
            reduceAvailableAppointmentCount(ab.getAppointment());
            appointmentBookingRepository.save(ab);
        }
        return appointmentBookings;
    }

    private void reduceAvailableAppointmentCount(Appointment appointment) throws ObjectNotFoundException {
        appointmentService.updateAppointmentCount(appointment.getId(), 0);
    }
}
