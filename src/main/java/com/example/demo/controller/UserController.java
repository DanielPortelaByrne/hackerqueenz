package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;

import com.example.demo.model.Appointment;
import com.example.demo.model.User;

import java.util.HashMap;
import java.util.List;

import com.example.demo.model.Question;
import com.example.demo.repos.QuestionRepository;
import com.example.demo.repos.UserRepository;
import com.example.demo.service.AppointmentService;
import com.example.demo.service.UserService;
import com.example.demo.service.UserValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private AppointmentService appointmentService;

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new User());

        return "registration";
    }

    @GetMapping("/statistics")
    public String statistics(Model model) {

        HashMap<String,Long> nationalies = new HashMap<String,Long>();

        // Gets count of all nations and adds to model
        for (String nation : userRepository.findAllNationalites()) {

                nationalies.put(nation,userRepository.countByNationality(nation));
        }
        model.addAttribute("Nationalities",nationalies);

        int from = 18;
        int to = 28;

        HashMap<String,Long> ages = new HashMap<String,Long>();
        while(from < 130) {

            long numberInCurrentAgeRange = userRepository.countByAgeRange(from, to);

            if(numberInCurrentAgeRange > 0) {
                ages.put(String.valueOf(from) + "-" + String.valueOf(to),numberInCurrentAgeRange);
            } 

            from += 10;
            to += 10;
        }
        model.addAttribute("ages",ages);

        model.addAttribute("numberOfMales",userRepository.countByGender("Male"));
        model.addAttribute("numberOfFemales",userRepository.countByGender("Female"));

        return "statistics";
    }

    @GetMapping("/questions")
    public String question(Model model, HttpServletRequest req) {
        String email = req.getUserPrincipal().getName();

        User user = null;

        if (!email.isEmpty()) {
            user = userRepository.findByEmail(email);
        }
        
            List<Question> questions = questionRepository.findAll();

            model.addAttribute("questionReq", new Question());

            model.addAttribute("user", user);

            model.addAttribute("questions", questions);

            return "questions";
    }

    @PutMapping("/questions")
    public ResponseEntity<Question> updateQuestion(Model model, HttpServletRequest req,@ModelAttribute("questionReq") Question questionReq) {
        
            if(req.isUserInRole("ROLE_ADMIN")) {

                try {
                    
                Question existingQuestion = questionRepository.findById(questionReq.getId()).get();

                existingQuestion.setAnswer(questionReq.getAnswer());

                questionRepository.save(existingQuestion);
                
                return ResponseEntity.ok(existingQuestion);
                
            } catch (Exception e) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            }

            return new ResponseEntity<>(HttpStatus.FORBIDDEN) ;
    }
    
    @PostMapping("/questions")
    public RedirectView addQuestionsOrAnswers(Model model, HttpServletRequest req, @ModelAttribute("questionReq") Question questionReq) {
        String email = req.getUserPrincipal().getName();

        User user = null;

        if (!email.isEmpty()) {
            user = userRepository.findByEmail(email);
        }


        if (req.isUserInRole("ROLE_ADMIN")) {

            model.addAttribute("user", user);

            return new RedirectView("/questions") ;
        } else {

            questionReq.setUser(user);
            Question newQuestion = questionRepository.save(questionReq);
            user.getQuestions().add(newQuestion);
            userRepository.save(user);

            return new RedirectView("/questions");
        }
    }

    @PostMapping("/registration")
    public RedirectView registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return new RedirectView("/registration");
        }

        userForm.setAge(userForm.calculateAge());
        userService.save(userForm);

        return new RedirectView("/welcome");
    }

    @GetMapping("/login")
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }

    @GetMapping("/confirmation")
    public String confirmation() {
        return "confirmation";
    }

    @GetMapping("/welcome")
    public RedirectView welcome(Model model, HttpServletRequest req) {
        return new RedirectView("/");
    }

    @GetMapping(value = { "", "/" })
    public String index(Model model) {
        List<Appointment> availableAppointments = appointmentService.getAllAvailableAppointments();
        model.addAttribute("appointments", availableAppointments);
        return "index";
    }

}