package com.example.demo.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.example.demo.common.BookingCart;
import com.example.demo.model.Appointment;
import com.example.demo.model.AppointmentBooking;
import com.example.demo.service.AppointmentBookingService;
import com.example.demo.service.AppointmentService;
import javassist.tools.rmi.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AppointmentController {
    @Autowired
    private AppointmentService appointmentService;
    @Autowired
    private AppointmentBookingService appointmentBookingService;

    @GetMapping("/appointment/{id}")
    public String viewAppointment(@PathVariable("id") String id, Model model) throws ObjectNotFoundException {
        model.addAttribute("appointment", appointmentService.getAppointment(id));
        return "appointment";
    }

    @GetMapping("/addappointment")
    public String addAppointment() {
        return "addappointment";
    }

    @RequestMapping("/addappointment/add")
    public String createNewAppointment(@RequestParam("name") String name, @RequestParam("centre") String centre,
                                       @RequestParam("date") String date, @RequestParam("time") String time, @RequestParam("availability") boolean availability)
            throws IOException {
        Appointment appointment = new Appointment();
        appointment.setName(name);
        appointment.setCentre(centre);
        appointment.setDate(date);
        appointment.setTime(time);
        appointment.setAvailability(availability);
        appointment = appointmentService.createAppointment(appointment);

        return "redirect:/appointment/" + appointment.getId();
    }

    @RequestMapping("/search")
    public String searchAppointments(@RequestParam("appointment-name") String appointmentName) {
        Appointment appointment = appointmentService.findByName(appointmentName.toUpperCase());
        if (appointment == null) {
            return "redirect:/appointment-not-found";
        }
        return "redirect:/appointment/" + appointment.getId();
    }

    @RequestMapping("/appointment/{id}/addToBookingCart")
    public String addToBookingCart(@PathVariable("id") String id, Model model,
                                   HttpSession session) throws ObjectNotFoundException {
        Appointment appointment = appointmentService.getAppointment(id);
        Map<String, AppointmentBooking> bookingCart = BookingCart.getBookingCart(session);
        AppointmentBooking appointmentBooking = appointmentBookingService.createAppointmentBooking(appointment);
        bookingCart.put(appointment.getId(), appointmentBooking);
        model.addAttribute("bookingCart", bookingCart);
        session.setAttribute("existingBookingCart", bookingCart);
        return "redirect:/bookingCart";
    }

    @RequestMapping(value = "/appointment/{id}/change-appointment")
    public String updateAppointment(@PathVariable("id") String id, @RequestParam("name") String name, @RequestParam("centre") String centre,
                                    @RequestParam("date") String date, @RequestParam("availability") boolean availability) throws ObjectNotFoundException {
        Appointment appointment = appointmentService.getAppointment(id);
        appointment.setName(name);
        appointment.setCentre(centre);
        appointment.setDate(date);
        appointment.setAvailability(availability);
        appointmentService.updateAppointment(appointment);
        return "redirect:/appointment/" + id.toString();
    }
}