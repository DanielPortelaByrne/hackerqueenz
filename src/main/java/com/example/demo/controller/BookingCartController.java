package com.example.demo.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import com.example.demo.common.BookingCart;
import com.example.demo.model.AppointmentBooking;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/bookingCart", method = RequestMethod.GET)
public class BookingCartController {
    @RequestMapping(value = { "", "/" })
    public String bookingCart(Model model, HttpSession session) {
        Map<String, AppointmentBooking> bookingCart = BookingCart.getBookingCart(session);
        model.addAttribute("bookingCart", bookingCart);
        session.setAttribute("existingBookingCart", bookingCart);
        model.addAttribute("count", BookingCart.getBookingCartCount(bookingCart));
        return "bookingCart";
    }

    @RequestMapping("/delete")
    public String deleteFromCart(@RequestParam("id") String id, Model model, HttpSession session) {
        Map<String, AppointmentBooking> bookingCart = BookingCart.getBookingCart(session);
        bookingCart.remove(id);
        model.addAttribute("bookingCart", bookingCart);
        model.addAttribute("count", BookingCart.getBookingCartCount(bookingCart));
        return "redirect:/bookingCart";
    }
}
