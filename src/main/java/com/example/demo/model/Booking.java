package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "bookings")
public class Booking {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(columnDefinition = "CHAR(32)")
    private String id;

    @Column(name = "date", updatable = false, nullable = false)
    private Date bookingDate;

    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private BookingStatus status;

    @OneToMany
    @JsonIgnore
    private List<AppointmentBooking> appointmentBookings = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    private User client;

    @OneToOne
    private Confirmation confirmation;

    private Vaccine vaccine;

    public Booking(String id, Date bookingDate, BookingStatus status, User client, Confirmation confirmation , Vaccine vaccine) {

        this.id = id;
        this.bookingDate = bookingDate;
        this.status = status;
        this.client = client;
    }

    public Booking() {
    }

    @Transient
    public String getId() {
        return this.id;
    }

    @Transient
    public User getClient() {
        return this.client;
    }

    @Transient
    public Date getBookingDate() {
        return this.bookingDate;
    }


    @Transient
    public BookingStatus getStatus() {
        return this.status;
    }

    @Transient
    public Confirmation getConfirmation() {
        return this.confirmation;
    }

    @Transient
    public List<AppointmentBooking> getAppointmentBookings() {
        return this.appointmentBookings;
    }

    @Transient
    public void setClient(User client) {
        this.client = client;
    }

    @Transient
    public void setBookingDate(Date bookingDate) {
        this.bookingDate = bookingDate;
    }

    @Transient
    public void setStatus(BookingStatus status) {
        this.status = status;
    }

    @Transient
    public void setVaccine(Vaccine vaccine) {
        this.vaccine = vaccine;
    }

    @Transient
    public Vaccine getVaccine() {return vaccine;}

    @Transient
    public void setConfirmation(Confirmation confirmation) {
        this.confirmation = confirmation;
    }


    @Transient
    public void addAppointmentBookingList(List<AppointmentBooking> appointmentBookings) {
        for (AppointmentBooking appointmentBooking : appointmentBookings) {
            this.appointmentBookings.add(appointmentBooking);
        }
    }

    @Transient
    public int getBookingSize() {
        return this.appointmentBookings.size();
    }
}
