package com.example.demo.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "appointments")
public class Appointment {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(columnDefinition = "CHAR(32)")
    private String id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String centre;

    @Column(nullable = false)
    private String date;

    @Column(nullable = false)
    private String time;

    @Column(nullable = false)
    private boolean availability;

    private int count;

    public Appointment(String id, String name, String centre, String date, String time, boolean availability) {
        this.id = id;
        this.name = name.toUpperCase();
        this.centre=centre;
        this.date = date;
        this.time = time;
        this.availability=availability;
        this.count=1; //count will always be 1 by default
    }

    public Appointment() {
    }

    @Transient
    public void setName(String name) {
        this.name = name.toUpperCase();
    }

    @Transient
    public void setDate(String date) {
        this.date = date;
    }

    @Transient
    public void setTime(String time) {
        this.time = time;
    }

    @Transient
    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    @Transient
    public void setAvailableAppointmentCount(int count) {
        this.count=count;
    }

    @Transient
    public void setCentre(String centre) {
        this.centre=centre;
    }

    @Transient
    public String getId() {
        return this.id;
    }

    @Transient
    public String getName() {
        return this.name;
    }

    @Transient
    public String getCentre() {
        return this.centre;
    }

    @Transient
    public String getDate(){
        return this.date;
    }

    @Transient
    public String getTime(){
        return this.time;
    }

    @Transient
    public boolean getAvailability(){
        return this.availability;
    }

    @Transient
    public int getAvailableAppointmentCount(){
        return this.count;
    }


}