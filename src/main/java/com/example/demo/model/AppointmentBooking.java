package com.example.demo.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.beans.Transient;

@Entity
@Table(name = "appointmentbookings")
public class AppointmentBooking {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(columnDefinition = "CHAR(32)")
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "appointment_id")
    private Appointment appointment;

    private String bookingId;

    @Column(nullable = false)
    private int appointmentCount;

    public AppointmentBooking(String id, Appointment appointment, String bookingId, int appointmentCount) {
        this.id = id;
        this.appointment = appointment;
    }

    public AppointmentBooking() {
    }

    @Transient
    public Appointment getAppointment() {
        return this.appointment;
    }

    @Transient
    public String getBookingId() {
        return this.bookingId;
    }

    @Transient
    public void setId(String id) {
        this.id = id;
    }

    @Transient
    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }

    @Transient
    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }
}

