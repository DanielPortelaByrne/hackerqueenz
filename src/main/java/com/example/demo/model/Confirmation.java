package com.example.demo.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "confirmations")
public class Confirmation {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(columnDefinition = "CHAR(32)")
    private String id;

    @OneToOne
    private Booking booking;

    public Confirmation(String id, Booking booking) {
        this.id = id;
        this.booking = booking;
    }

    public Confirmation() {
    }

    @Transient
    public String getId() {
        return this.id;
    }

    @Transient
    public Booking getBooking() {
        return this.booking;
    }

    @Transient
    public void setBooking(Booking booking) {
        this.booking = booking;
    }

}
