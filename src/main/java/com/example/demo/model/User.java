package com.example.demo.model;

import javax.persistence.*;

import org.hibernate.annotations.ColumnTransformer;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "users")
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ColumnTransformer(
    read = "cast(aes_decrypt(first_name, 'my secret') as char(255))", 
    write = "aes_encrypt(?, 'my secret')"    
    )  
    @Column(nullable = true, length = 255)
    private String firstName;
    @Column(nullable = true, length = 255)
    @ColumnTransformer(
    read = "cast(aes_decrypt(last_name, 'my secret') as char(255))", 
    write = "aes_encrypt(?, 'my secret')"    
    )  
    private String lastName;
    @Column(nullable = true, length = 255)
    private String password;
    @Column(nullable = true, length = 255)
    @ColumnTransformer(
    read = "cast(aes_decrypt(password_confirm, 'my secret') as char(255))", 
    write = "aes_encrypt(?, 'my secret')"    
    )  
    private String passwordConfirm;
    @Column(nullable = true, length = 255)
    private String role;
    @ColumnTransformer(
    read = "cast(aes_decrypt(address, 'my secret') as char(255))", 
    write = "aes_encrypt(?, 'my secret')"    
    )  
    @Column(nullable = true, length = 255)
    private String address;
    @ColumnTransformer(
    read = "cast(aes_decrypt(email, 'my secret') as char(255))", 
    write = "aes_encrypt(?, 'my secret')"    
    )  
    @Column(nullable = true, length = 255)
    private String email;
    @Column(nullable = true, length = 255)
    private String nationality;
    @Column(nullable = true)
    private String gender;
    @Column(nullable = true)
    private int age;
    
    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @ColumnTransformer(
        read = "cast(aes_decrypt(pps_number, 'my secret') as char(255))", 
        write = "aes_encrypt(?, 'my secret')"   
    )  
    private String ppsNumber;
    @ColumnTransformer(
    read = "cast(aes_decrypt(phone_number, 'my secret') as char(255))", 
    write = "aes_encrypt(?, 'my secret')"    
    )       
    @Column(nullable = true, length = 255)
    private String phoneNumber;
    @Column
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) 
    private LocalDate dateOfBirth;
    
    @OneToMany(mappedBy="user")
    private List<Question> questions;
    
    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public Long getId() {
        return id;
    }

    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Transient
    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public int calculateAge() {

        if(dateOfBirth == null) {
            return -1;
        }
        
        return LocalDate.now().getYear() - dateOfBirth.getYear();

    }

    @Override
    @ElementCollection(targetClass = String.class)
    @Transient
    public Collection<? extends GrantedAuthority> getAuthorities() {

        List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();

        list.add(new SimpleGrantedAuthority("ROLE_" + role));

        return list;
    }

    @Override
    @Transient
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @Transient
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @Transient
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @Transient
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getUsername() {
        return email;
    }

    public String getPpsNumber() {
        return ppsNumber;
    }

    public void setPpsNumber(String ppsNumber) {
        this.ppsNumber = ppsNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}