package com.example.demo.repos;

import com.example.demo.model.AppointmentBooking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppointmentBookingRepository extends JpaRepository<AppointmentBooking, Long> {

}
