package com.example.demo.repos;

import java.util.List;

import com.example.demo.model.User;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    User findByEmail(String email);

    @Query("SELECT COUNT(u) FROM User u WHERE u.gender=:gender")
    long countByGender(@Param("gender") String gender);

    @Query("SELECT COUNT(u) FROM User u WHERE u.age >= :from AND u.age < :to")
    long countByAgeRange(int from,int to);

    @Query("SELECT COUNT(u) FROM User u WHERE u.nationality=?1")
    long countByNationality(String nationality);

    @Query("SELECT DISTINCT u.nationality FROM User u ")
    List<String> findAllNationalites();
}