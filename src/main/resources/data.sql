INSERT INTO appointments(id, name, centre, date, time, availability, count)
VALUES(2000000, 'FIRST DOSE', 'Croke Park', '12/08/2021', '15:00', TRUE, 1);

INSERT INTO appointments(id, name, centre, date, time, availability, count)
VALUES(2000001, 'FIRST DOSE', 'Croke Park', '12/08/2021', '17:00', TRUE, 1);

INSERT INTO appointments(id, name, centre, date, time, availability, count)
VALUES(2000002, 'FIRST DOSE', 'Croke Park', '14/08/2021', '15:30', TRUE, 1);

INSERT INTO appointments(id, name, centre, date, time, availability, count)
VALUES(2000003, 'FIRST DOSE', 'Croke Park', '14/08/2021', '19:00', TRUE, 1);

INSERT INTO appointments(id, name, centre, date, time, availability, count)
VALUES(2000004, 'SECOND DOSE', 'Croke Park', '12/12/2021', '15:00', TRUE, 1);

INSERT INTO appointments(id, name, centre, date, time, availability, count)
VALUES(2000005, 'SECOND DOSE', 'Croke Park', '12/12/2021', '17:00', TRUE, 1);

INSERT INTO appointments(id, name, centre, date, time, availability, count)
VALUES(2000006, 'SECOND DOSE', 'Croke Park', '14/12/2021', '15:30', TRUE, 1);

INSERT INTO appointments(id, name, centre, date, time, availability, count)
VALUES(2000007, 'SECOND DOSE', 'Croke Park', '14/12/2021', '19:00', TRUE, 1);

INSERT INTO appointments(id, name, centre, date, time, availability, count)
VALUES(3000000, 'FIRST DOSE', 'UCD', '01/03/2022', '15:00', TRUE, 1);

INSERT INTO appointments(id, name, centre, date, time, availability, count)
VALUES(3000001, 'FIRST DOSE', 'UCD', '01/03/2022', '17:00', TRUE, 1);

INSERT INTO appointments(id, name, centre, date, time, availability, count)
VALUES(3000002, 'FIRST DOSE', 'UCD', '05/03/2022', '13:00', TRUE, 1);

INSERT INTO appointments(id, name, centre, date, time, availability, count)
VALUES(3000003, 'FIRST DOSE', 'UCD', '05/03/2022', '16:00', TRUE, 1);

INSERT INTO appointments(id, name, centre, date, time, availability, count)
VALUES(3000004, 'SECOND DOSE', 'UCD', '01/03/2022', '15:00', TRUE, 1);

INSERT INTO appointments(id, name, centre, date, time, availability, count)
VALUES(3000005, 'SECOND DOSE', 'UCD', '01/03/2022', '17:00', TRUE, 1);

INSERT INTO appointments(id, name, centre, date, time, availability, count)
VALUES(3000006, 'SECOND DOSE', 'UCD', '05/05/2022', '13:00', TRUE, 1);

INSERT INTO appointments(id, name, centre, date, time, availability, count)
VALUES(3000007, 'SECOND DOSE', 'UCD', '05/05/2022', '16:00', TRUE, 1);

INSERT INTO users(ID,ADDRESS,DATE_OF_BIRTH,EMAIL,FIRST_NAME,LAST_NAME,NATIONALITY,PASSWORD,PASSWORD_CONFIRM,PHONE_NUMBER,PPS_NUMBER,ROLE,AGE,GENDER)
VALUES(1,aes_encrypt('Random Address', 'my secret'),'1940-10-31',aes_encrypt('user@gmail.com', 'my secret'),aes_encrypt('Peter', 'my secret'),aes_encrypt('ODonnell', 'my secret'),aes_encrypt('Irish', 'my secret'),'$2a$10$pC18921K8rJIf5T4v./IbuK02mKLKndO0Xi2SDYamp46YfvT4Hv0K',aes_encrypt('$2a$10$pC18921K8rJIf5T4v./IbuK02mKLKndO0Xi2SDYamp46YfvT4Hv0K', 'my secret'),aes_encrypt('0838376683', 'my secret'),aes_encrypt('123456789', 'my secret'),'USER',19,'Female');

INSERT INTO users(ID,ADDRESS,DATE_OF_BIRTH,EMAIL,FIRST_NAME,LAST_NAME,NATIONALITY,PASSWORD,PASSWORD_CONFIRM,PHONE_NUMBER,PPS_NUMBER,ROLE,AGE,GENDER)
VALUES(2,aes_encrypt('Random Address', 'my secret'),'1940-10-31',aes_encrypt('admin@gmail.com', 'my secret'),aes_encrypt('Peter', 'my secret'),aes_encrypt('ODonnell', 'my secret'),aes_encrypt('Irish', 'my secret'),'$2a$10$pC18921K8rJIf5T4v./IbuK02mKLKndO0Xi2SDYamp46YfvT4Hv0K',aes_encrypt('$2a$10$pC18921K8rJIf5T4v./IbuK02mKLKndO0Xi2SDYamp46YfvT4Hv0K', 'my secret'),aes_encrypt('0838376683', 'my secret'),aes_encrypt('123456789', 'my secret'),'ADMIN',19,'Female');

-- INSERT INTO users(ID,ADDRESS,DATE_OF_BIRTH,EMAIL,FIRST_NAME,LAST_NAME,NATIONALITY,PASSWORD,PASSWORD_CONFIRM,PHONE_NUMBER,PPS_NUMBER,ROLE,AGE,GENDER)
-- VALUES(2,'Random Address','1950-10-31','admin@gmail.com','Peter','ODonnell','English','$2a$10$pC18921K8rJIf5T4v./IbuK02mKLKndO0Xi2SDYamp46YfvT4Hv0K','$2a$10$pC18921K8rJIf5T4v./IbuK02mKLKndO0Xi2SDYamp46YfvT4Hv0K','0838376683','123456789','ADMIN',29,'Male');

-- INSERT INTO users(ID,ADDRESS,DATE_OF_BIRTH,EMAIL,FIRST_NAME,LAST_NAME,NATIONALITY,PASSWORD,PASSWORD_CONFIRM,PHONE_NUMBER,PPS_NUMBER,ROLE,AGE,GENDER)
-- VALUES(1,"aes_encrypt('Random Address','my secret')",'1940-10-31','user@gmail.com','Peter','ODonnell','Irish','$2a$10$pC18921K8rJIf5T4v./IbuK02mKLKndO0Xi2SDYamp46YfvT4Hv0K','$2a$10$pC18921K8rJIf5T4v./IbuK02mKLKndO0Xi2SDYamp46YfvT4Hv0K','0838376683','123456789','USER',19,'Female');

-- INSERT INTO users(ID,ADDRESS,DATE_OF_BIRTH,EMAIL,FIRST_NAME,LAST_NAME,NATIONALITY,PASSWORD,PASSWORD_CONFIRM,PHONE_NUMBER,PPS_NUMBER,ROLE,AGE,GENDER)
-- VALUES(2,'Random Address','1950-10-31','admin@gmail.com','Peter','ODonnell','English','$2a$10$pC18921K8rJIf5T4v./IbuK02mKLKndO0Xi2SDYamp46YfvT4Hv0K','$2a$10$pC18921K8rJIf5T4v./IbuK02mKLKndO0Xi2SDYamp46YfvT4Hv0K','0838376683','123456789','ADMIN',29,'Male');

INSERT into questions(question,answer,user_id) 
VALUES('This is a sample question','This is a sample answer',1);

INSERT into questions(question,answer,user_id) 
VALUES('This is a sample question','This is a sample answer',1);

INSERT into questions(question,answer,user_id) 
VALUES('This is a sample question','This is a sample answer',1);

INSERT into questions(question,answer,user_id) 
VALUES('This is a sample question','This is a sample answer',1);

INSERT into questions(question,answer,user_id) 
VALUES('This is a sample question','This is a sample answer',1);

INSERT into questions(question,answer,user_id) 
VALUES('This is a sample question','This is a sample answer',1);

INSERT into questions(question,answer,user_id) 
VALUES('This is a sample question',null,1);